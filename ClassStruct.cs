using System;

namespace SampleApp
{
    public class PassportClass
    {
        public string passportNumber;
        public DateTime expiration;  // DateTime.Parse("2022-01-18")
        public string country;
    }
    
    public struct PassportStruct
    {
        public string passportNumber;
        public DateTime expiration;  // DateTime.Parse("2022-01-18")
        public string country;
    }

    public class ClassStruct
    {
        public static void ClassStructSample()
        {
            PassportClass passportClass = null;
            PassportStruct passportStruct;  // can not set null

            Console.WriteLine($"Before initialization: '{passportClass}'");
            //Console.WriteLine($"Before initialization: {passportStruct}");  // can not access before initialization

            passportClass = new PassportClass { 
                passportNumber = "123",
                expiration = DateTime.Now + TimeSpan.FromDays(2000),
                country = "Ukraine"
            };

            passportStruct = new PassportStruct { 
                passportNumber = "321",
                expiration = DateTime.Now + TimeSpan.FromDays(5000),
                country = "Ukraine"
            };

            Console.WriteLine($"Initial number in class: '{passportClass.passportNumber}'");
            Console.WriteLine($"Initial number is struct: {passportStruct.passportNumber}'");

            PassportClass passportClass2 = passportClass;
            PassportStruct passportStruct2 = passportStruct;  // can not set null

            passportClass2.passportNumber = "123999";
            passportStruct2.passportNumber = "321999";

            Console.WriteLine($"After changing passportClass2: '{passportClass.passportNumber}'");
            Console.WriteLine($"After changing passportStruct2: '{passportStruct.passportNumber}'");

            Method(passportClass, passportStruct);
        }

        public static void Method(PassportClass pc, PassportStruct ps)
        {

        }
    }
}