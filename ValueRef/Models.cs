using System;

namespace SampleApp
{
    public interface IPoint
    {
        long X { get; }
        long Y { get; }
    }

    public class PointClass : IPoint
    {
        private long x;
        private long y;
        public PointClass(long xValue, long yValue)
        {
            x = xValue;
            y = yValue;
        }
        public long X { get => x; }
        public long Y { get => y; }
    }
    
    public struct PointStruct : IPoint
    {
        private long x;
        private long y;
        public PointStruct(long xValue, long yValue)
        {
            x = xValue;
            y = yValue;
        }
        public long X { get => x; }
        public long Y { get => y; }
    }
}