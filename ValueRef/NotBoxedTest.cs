using System;

namespace SampleApp
{
    public class NotBoxedClassTest
    {
        public static void RunTest(int scale)
        {
            PointClass[] points = new PointClass[scale];

            for(int i = 0; i < scale; i++)
            {
                points[i] = new PointClass(i, i);
            }

            for(int i = 0; i < scale; i++)
            {
                points[i] = MovePoint(points[i], -i, -i);
            }

            int xSum = 0, ySum = 0;
            for(int i = 0; i < scale; i++)
            {
                points[i] = MovePoint(points[i], -i, -i);
            }

            Console.WriteLine($"XSum = {xSum}, YSum = {ySum}");
        }

        private static PointClass MovePoint(PointClass point, int xShift, int yShift)
        {
             return new PointClass(point.X + xShift, point.Y + yShift);
        }
    }
    public class NotBoxedStructTest
    {
        public static void RunTest(int scale)
        {
            PointStruct[] points = new PointStruct[scale];

            for(int i = 0; i < scale; i++)
            {
                points[i] = new PointStruct(i, i);
            }

            for(int i = 0; i < scale; i++)
            {
                points[i] = MovePoint(points[i], -i, -i);
            }

            int xSum = 0, ySum = 0;
            for(int i = 0; i < scale; i++)
            {
                points[i] = MovePoint(points[i], -i, -i);
            }
            
            Console.WriteLine($"XSum = {xSum}, YSum = {ySum}");
        }

        private static PointStruct MovePoint(PointStruct point, int xShift, int yShift)
        {
             return new PointStruct(point.X + xShift, point.Y + yShift);
        }
    }
}