using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SampleApp 
{
    public class TestRunner
    {
        private static IDictionary<string, Action<int>> Tests = new Dictionary<string, Action<int>>
        {
            { "BoxedClassTest", scale => BoxedClassTest.RunTest(scale) },
            { "BoxedStructTest", scale => BoxedStructTest.RunTest(scale) },
            { "NotBoxedClassTest", scale => NotBoxedClassTest.RunTest(scale) },
            { "NotBoxedStructTest", scale => NotBoxedStructTest.RunTest(scale) },
            { "RefClassTest", scale => RefClassTest.RunTest(scale) },
            { "RefStructTest", scale => RefStructTest.RunTest(scale) },
        };
        public static void Run(int scale, string testName)
        {
            var currentProcess = Process.GetCurrentProcess();
            var initialVirtualMemory = currentProcess.VirtualMemorySize64;
            var initialWorkingSet = currentProcess.WorkingSet64;
            
            var testNames = testName == "all" ? Tests.Keys : new List<string> { testName };
            
            foreach(var name in testNames)
            {
                var test = Tests[name];
                var start = currentProcess.TotalProcessorTime;
                GC.Collect();
                var allocated = GC.GetTotalAllocatedBytes;
                Task.Delay(TimeSpan.FromSeconds(5));

                BoxedClassTest.RunTest(scale);

                Console.WriteLine(name);
                Console.WriteLine($"  CPU time spent:       {currentProcess.TotalProcessorTime - start}");
                Console.WriteLine($"  Extra virtual memory: {initialVirtualMemory - currentProcess.VirtualMemorySize64}");
                Console.WriteLine($"  Extra working set:    {initialWorkingSet - currentProcess.WorkingSet64}");
                Console.WriteLine($"  Extra allocated:      {allocated - GC.GetTotalAllocatedBytes}");
            }
        }
    }
}