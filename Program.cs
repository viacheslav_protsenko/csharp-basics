﻿using System;

namespace SampleApp
{
  class Program
  {
    static void Main(string[] args)
    {
      // for name in BoxedClassTest BoxedStructTest NotBoxedClassTest NotBoxedStructTest RefClassTest RefStructTest ; do dotnet run 6000000 $name ; done
      TestRunner.Run(int.Parse(args[0]), args[1]);

      // FlowControl.FlowControlSample();
      
      // ClassStruct.ClassStructSample();
      
      // HelloName();

      Console.Read();
    }

    static void HelloName()
    {
      Console.WriteLine("What is your name?");
      var name = Console.ReadLine();
      Console.WriteLine($"Nice to meet you {name}!");
    }
  }
}
